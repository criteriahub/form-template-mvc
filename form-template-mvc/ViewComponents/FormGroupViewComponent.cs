﻿using form_template_mvc.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using form_template_mvc.Library.Form;

namespace form_template_mvc.ViewComponents
{
    [ViewComponent(Name = "FormGroup")]
    public class FormGroupViewComponent : ViewComponent
    {
        protected readonly IFormInputRepository _factory;

        public FormGroupViewComponent(IFormInputRepository factory)
        {
            _factory = factory;
        }

        /// <summary>
        /// Renders a group of inputs based on the given group/topic
        /// </summary>
        /// <param name="fields">List of all form data fields</param>
        /// <param name="group">What group/topic to filter on</param>
        /// <param name="title">Title of the rendered bootstrap card</param>
        public async Task<IViewComponentResult> InvokeAsync(IEnumerable<FormData> fields, string group, string title)
        {
            return await Task.Run(() => {
                List<InputModel> model = new();

                // If there was any ordering properties on the FormData we would sort then here.
                // For now just go with the order the api served up which is in db creation order.

                // Only used fields with the given group.
                foreach (FormData d in fields.ToList().Where(f => f.Topic == group))
                {
                    model.Add(_factory.CreateFormInput(d, group));
                }

                return View(new FormGroupModel()
                {
                    Title = title,
                    Data = model,
                });
            });
        }
    }

    public class FormGroupModel
    {
        public string Title { get; set; }
        public IEnumerable<InputModel> Data { get; set; }
    }
}
