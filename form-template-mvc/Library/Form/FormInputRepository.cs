﻿using System;
using form_template_mvc.Models;

namespace form_template_mvc.Library.Form
{
    public class FormInputRepository : IFormInputRepository
    {
        public InputModel CreateFormInput(FormData data, string group)
        {
            return data.InputType switch
            {
                "text" => new TextInput(data, group),
                "date" => new TextInput(data, group),
                "select" => new SelectInput(data, group),
                // Todo: create input models for the below ones.
                "currency" => new TextInput(data, group),
                "radio" => new TextInput(data, group),
                "radio_group" => new TextInput(data, group),
                "checkbox" => new TextInput(data, group),
                _ => null
            };
        }
    }
}
