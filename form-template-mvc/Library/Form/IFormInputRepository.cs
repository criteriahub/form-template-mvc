﻿using System;
using form_template_mvc.Models;

namespace form_template_mvc.Library.Form
{
    public interface IFormInputRepository
    {
        public InputModel CreateFormInput(FormData data, string group);
    }
}
