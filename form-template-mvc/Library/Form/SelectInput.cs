﻿using System;
using System.Collections.Generic;
using form_template_mvc.Models;

namespace form_template_mvc.Library.Form
{
    public class SelectInput : InputModel
    {
        public SelectInput(FormData data, string group) : base(data, group) { }

        public Dictionary<string, string> GetOptions()
        {
            Dictionary<string, string> options = new() { { "", "Please select..." } };

            if (String.IsNullOrEmpty(_data.SelectValues) || String.IsNullOrWhiteSpace(_data.SelectValues))
            {
                return options;
            }

            string loopType = this.getLoopType(_data.SelectValues);

            if (loopType == "range")
            {
                // TODO
            } else if (loopType == "exact")
            {
                var valuesString = this.getLoopValues(_data.SelectValues);
                if (!String.IsNullOrEmpty(valuesString))
                {
                    foreach (var option in this.getOptionParts(valuesString))
                    {
                        var parts = option.Trim().Split("=>");
                        options.Add(parts[0].Trim(), parts[1].Trim());
                    }
                }
            }

            return options;
        }

        protected string getLoopType(string valueString)
        {
            int pipeLocation = this.pipeLocation(valueString);
            if (pipeLocation > 0)
            {
                return valueString.Substring(0, pipeLocation);
            }
            return String.Empty;
        }

        protected string getLoopValues(string valueString)
        {
            int pipeLocation = this.pipeLocation(valueString);
            if (pipeLocation > 0)
            {
                return valueString.Substring(pipeLocation + 1, (valueString.Length - pipeLocation) - 1);
            }
            return String.Empty;
        }

        protected int pipeLocation(string valueString)
        {
            return valueString.IndexOf("|", StringComparison.Ordinal);
        }

        protected string[] getOptionParts(string valueString)
        {
            return valueString.Split(",");
        }
    }
}
