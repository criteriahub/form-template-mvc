﻿using System;
using form_template_mvc.Models;

namespace form_template_mvc.Library.Form
{
    public class InputModel
    {
        protected readonly FormData _data;
        protected readonly string _group;


        public InputModel(FormData data, string group)
        {
            _data = data;
            _group = group;
        }

        public enum InputType
        {
            INVALID,
            TEXT,
            DATE,
            CURRENCY,
            SELECT,
            RADIO,
            RADIOGROUP,
            CHECKBOX
        }

        public string GetID()
        {
            return _data.ID;
        }

        public string GetLabelText()
        {
            return _data.Name;
        }

        public string GetNameAttribute()
        {
            return $"{_group}[{_data.ID}]";
        }

        public InputType GetInputType()
        {
            return _data.InputType switch
            {
                "text" => InputType.TEXT,
                "date" => InputType.DATE,
                "currency" => InputType.CURRENCY,
                "select" => InputType.SELECT,
                "radio" => InputType.RADIO,
                "radio_group" => InputType.RADIOGROUP,
                "checkbox" => InputType.CHECKBOX,
                _ => InputType.INVALID,
            };
        }
    }
}
