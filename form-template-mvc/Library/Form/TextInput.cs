﻿using System;
using form_template_mvc.Models;

namespace form_template_mvc.Library.Form
{
    public class TextInput : InputModel
    {
        public TextInput(FormData data, string group) : base(data, group) { }


        public bool IsRequired()
        {
            return _data.IsRequired;
        }
    }
}
