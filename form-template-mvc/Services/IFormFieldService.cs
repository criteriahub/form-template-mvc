﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using form_template_mvc.Models;

namespace form_template_mvc.Services
{
    public interface IFormFieldService
    {
        public Task<IEnumerable<FormData>> GetFormFieldsAsync();
    }
}
