﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using form_template_mvc.Models;

namespace form_template_mvc.Services
{
    public class FormFieldService : IFormFieldService
    {
        private readonly HttpClient _client;

        public FormFieldService(HttpClient client)
        {
            client.BaseAddress = new Uri("https://webhooks.mongodb-realm.com/");
            //client.DefaultRequestHeaders.Add("Content-Type", "application/json");
            _client = client;
        }

        public async Task<IEnumerable<FormData>> GetFormFieldsAsync()
        {
            return await _client.GetFromJsonAsync<IEnumerable<FormData>>("/api/client/v2.0/app/crm-xfrdp/service/http/incoming_webhook/questions");
        }
    }
}
