﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using form_template_mvc.Models;
using form_template_mvc.Services;

namespace form_template_mvc.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IFormFieldService _formsvc;

        public HomeController(ILogger<HomeController> logger, IFormFieldService formsvc)
        {
            _logger = logger;
            _formsvc = formsvc;
        }

        public async Task<IActionResult> Index()
        {
            var fields = await _formsvc.GetFormFieldsAsync();
            return View(fields);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
