﻿using System;
using System.Text.Json.Serialization;


namespace form_template_mvc.Models
{
    public class FormData
    {
        /// <summary>
        /// A unique ID that can be used for form input name attributes.
        /// </summary>
        [JsonPropertyName("form_id")]
        public string ID { get; set; }

        /// <summary>
        /// Question name is for used for form input labels
        /// </summary>
        [JsonPropertyName("question_name")]
        public string Name { get; set; }

        /// <summary>
        /// Default value to either apply to the form input or use if null value
        /// received from the form data.
        /// </summary>
        [JsonPropertyName("question_default_value")]
        public string DefaultValue { get; set; }

        /// <summary>
        /// Can be used as an alternative to ID as a unique identifier in
        /// a format thats nicer on the eyes
        /// </summary>
        [JsonPropertyName("code_name")]
        public string CodeName { get; set; }

        /// <summary>
        /// Tooltop message to display if set
        /// </summary>
        [JsonPropertyName("question_tooltip")]
        public string ToolTip { get; set; }

        /// <summary>
        /// Topic is the group of inputs a question belongs to.
        ///
        /// Such as property or applicant
        /// </summary>
        [JsonPropertyName("question_topic")]
        public string Topic { get; set; }

        /// <summary>
        /// Used to make decisions on input type to render for a field
        /// </summary>
        [JsonPropertyName("setting_formtype")]
        public string InputType { get; set; }

        /// <summary>
        /// The expected data type
        /// </summary>
        [JsonPropertyName("setting_datatype")]
        public string DataType { get; set; }

        /// <summary>
        /// String representation of the select dropdowns values.
        ///
        /// Note: Requires and intermediary step to be turned into a Dictionary
        /// to make it usable.
        /// </summary>
        [JsonPropertyName("setting_values")]
        public string SelectValues { get; set; }

        /// <summary>
        /// Is the input a required field. Used to set an inputs required attribute
        /// </summary>
        [JsonPropertyName("setting_required")]
        public bool IsRequired { get; set; }

        /// <summary>
        /// String representation of the validation rules to use.
        ///
        /// Split on pipe char in order to iterate on them
        /// </summary>
        [JsonPropertyName("setting_validation")]
        public string Validation { get; set; }
        
    }
}
